export const validateName = (name) => {
    let err = null;

    if (!name) err = 'Name is required';
    if (name.length > 200) err = 'length should not be greater than 200';

    return err;
}

export const validateCategories = (categories) => {
    let err = null;

    if (!categories.length) err = 'Category is required';
    if (categories.length > 5) err = 'The maximum number of categories is 5';

    return err;
}

export const validateExpirationDate = (expirationDate) => {
    const day = 1000 * 60 * 60 * 24;
    let err = null;

    if (expirationDate && Math.abs(expirationDate - new Date()) / day < 30)
        err = 'should expire not less than 30 days since now';

    return err;
}

