import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardText, CardTitle, ListGroup, ListGroupItem, ButtonGroup } from 'reactstrap';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { deleteProduct } from '../../actions/products';
import { connect } from 'react-redux';


const shortDateFormat = 'MM/DD/YYYY';
const longDateFormat = 'MM/DD/YYYY hh:mm a';

const Product = ({ product, dispatch }) => {
    const receiptDate = product.receiptDate
        ? moment(product.receiptDate).format(shortDateFormat)
        : "-";
    const expirationDate = product.expirationDate
        ? moment(product.expirationDate).format(shortDateFormat)
        : "-";
    const createdAt = product.createdAt
        ? moment(product.createdAt).format(longDateFormat)
        : "-";

    return (
        <Card>
            <CardBody>
                <CardTitle className="mr-auto">{product.name}</CardTitle>
                <CardText tag="div" className="mb-4">
                    <ListGroup>
                        <ListGroupItem>Brand: {product.brand}</ListGroupItem>
                        <ListGroupItem>Rating: {product.rating}</ListGroupItem>
                        <ListGroupItem>Featured: {product.featured ? 'Yes' : 'No'}</ListGroupItem>
                        <ListGroupItem>Items In Stock: {product.itemsInStock}</ListGroupItem>
                        <ListGroupItem>
                            Categories:
                            <ul>
                                {product.categories.map((category) => (
                                    <li key={category.id}>{category.name}</li>
                                ))}
                            </ul>
                        </ListGroupItem>
                        <ListGroupItem>Receipt Date: {receiptDate}</ListGroupItem>
                        <ListGroupItem>Expiration Date: {expirationDate}</ListGroupItem>
                        <ListGroupItem>Created At: {createdAt}</ListGroupItem>
                    </ListGroup>
                </CardText>
                <ButtonGroup>
                    <Link className="btn btn-primary" to={`/edit/${product.id}`}>
                        Edit
                    </Link>
                    <button
                        className="btn btn-danger"
                        onClick={() => dispatch(deleteProduct(product.id))}
                    >
                        Delete
                    </button>
                </ButtonGroup>
            </CardBody>
        </Card>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.products,
    };
};

Product.propTypes = {
    product: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(Product);
