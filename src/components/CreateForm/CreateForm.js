import React, { PureComponent } from 'react';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { validateName, validateCategories, validateExpirationDate } from '../../utils';
import { connect } from 'react-redux';
import { fetchCategories } from '../../actions/categories';
import { addProduct } from '../../actions/products';
import { Link } from "react-router-dom";


Moment.locale("en");
momentLocalizer();

class CreateForm extends PureComponent {
    state = {
        receiptDate: new Date(),
        expirationDate: new Date(),
        selectedCategories: [],
        errName: null,
        errCategories: null,
        errDateExpiration: null,
        isProductCreated: false
    }

    componentDidMount() {
        this.props.dispatch(fetchCategories());
    }

    handleCategoryChange = event => {
        const options = event.target.options;
        const categoryIDs = [];

        for (let i = 0; i < options.length; i++) {
            if (options[i].selected) {
                categoryIDs.push(Number(options[i].id));
            }
        }

        this.setState({ selectedCategories: categoryIDs })
    }

    handleSubmit = () => {
        const { dispatch } = this.props;
        const { selectedCategories, expirationDate } = this.state;
        const name = this.inputName.value;
        const errName = validateName(name);
        const errCategories = validateCategories(selectedCategories);
        const errDateExpiration = validateExpirationDate(expirationDate);

        if (errName || errCategories || errDateExpiration) {
            this.setState({ errName, errCategories, errDateExpiration });
        } else {
            dispatch(addProduct({
                id: `${(+new Date()).toString()}`,
                name,
                rating: this.inputRating.value,
                featured: this.inputRating.value > 8,
                itemsInStock: this.inputNumberInStock.value,
                receiptDate: this.state.receiptDate,
                brand: this.inputBrand.value,
                categories: selectedCategories,
                expirationDate,
                createdAt: new Date().toISOString(),
            }));

            this.setState({ errName, errCategories, errDateExpiration, isProductCreated: true });
        }
    };

    renderRatingOptions = () => {
        const optionsLength = [...Array(9).keys()];

        return optionsLength.map(o =>
            <option value={o + 1} key={`option-${o + 1}`}>
                {o + 1}
            </option>
        );
    }

    render() {
        const { categories } = this.props;
        const { errName, errCategories, errDateExpiration, isProductCreated } = this.state;

        return (
            <div className="mb-5">
                <h2 className="h3">Create product</h2>
                <hr/>
                <Form className="product-form">
                    <FormGroup>
                        <Label for='pName'>Name*</Label>
                        <Input
                            name='name'
                            id='pName'
                            innerRef={(node) => this.inputName = node}
                        />
                        <FormText color="danger">
                            {errName}
                        </FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label for='pBrand'>Brand</Label>
                        <Input
                            name='brand'
                            id='pBrand'
                            innerRef={(node) => this.inputBrand = node}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for='pRating'>Rating</Label>
                        <Input
                            type='select'
                            name='rating'
                            id='pRating'
                            innerRef={(node) => this.inputRating = node}
                        >
                            {this.renderRatingOptions()}
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for='pStock'>Number in stock</Label>
                        <Input
                            type='number'
                            defaultValue='0'
                            name='stock'
                            id='pStock'
                            innerRef={(node) => this.inputNumberInStock = node}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for='pCategories'>Categories*</Label>
                        <Input
                            type='select'
                            name='categories'
                            id='pCategories'
                            onChange={this.handleCategoryChange}
                            multiple={true}
                        >
                            {categories.map((category) => (
                                <option key={category.id} id={category.id}>{category.name}</option>
                            ))}
                        </Input>
                        <FormText color="danger">
                            {errCategories}
                        </FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Receipt Date</Label>
                        <DateTimePicker
                            time={false}
                            value={this.state.receiptDate}
                            onChange={value => this.setState({ receiptDate: value })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Expiration Date</Label>
                        <DateTimePicker
                            time={false}
                            value={this.state.expirationDate}
                            onChange={value => this.setState({ expirationDate: value })}
                        />
                        <FormText color="danger">
                            {errDateExpiration}
                        </FormText>
                    </FormGroup>
                    {isProductCreated
                        ? <div>
                            <p>The product's created!</p>
                            <Link id="backLink" className="product-form" to="/">Back</Link>
                        </div>
                        : <Button id="createBtn" className="btn btn-success" onClick={this.handleSubmit}>
                            Create
                        </Button>
                    }
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories,
    };
};

export default connect(mapStateToProps)(CreateForm);
