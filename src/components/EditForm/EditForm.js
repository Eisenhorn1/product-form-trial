import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Col, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import Moment from 'moment';
import { validateName, validateCategories, validateExpirationDate } from '../../utils';
import { fetchCategories } from '../../actions/categories';
import { fetchProductById, requestSaveProduct } from '../../actions/products';
import { Link } from 'react-router-dom';


Moment.locale('en');

class EditForm extends PureComponent {
    state = {
        receiptDate: new Date(),
        expirationDate: new Date(),
        selectedCategories: [],
        name: '',
        brand: '',
        rating: 1,
        featured: false,
        itemsInStock: 10,
        errName: null,
        errCategories: null,
        errDateExpiration: null,
        isProductChanged: false,
    };

    componentDidMount() {
        const { dispatch, match } = this.props;
        const productId = match.params.id;

        dispatch(fetchCategories());
        dispatch(fetchProductById(productId));
    }

    componentDidUpdate(prevProps) {
        const { selectedProduct: prevSelectedProduct, match: prevMatch } = prevProps;
        const { selectedProduct, match, dispatch } = this.props;
        const productId = match.params.id;
        const prevProductId = prevMatch.params.id;

        if (productId !== prevProductId) {
            dispatch(fetchProductById(productId));
        }

        if (
            JSON.stringify(selectedProduct) !== JSON.stringify(prevSelectedProduct)
        ) {
            const {
                name,
                rating,
                itemsInStock,
                receiptDate,
                brand,
                expirationDate,
                categories,
            } = selectedProduct;

            this.setState({
                name,
                brand,
                rating,
                featured: rating > 8,
                itemsInStock,
                expirationDate: expirationDate ? new Date(expirationDate) : null,
                selectedCategories: categories,
                receiptDate: receiptDate ? new Date(receiptDate) : null,
            });
        }
    }

    handleInputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleCategoryChange = event => {
        const options = event.target.options;
        const categoryIDs = [];

        for (let i = 0; i < options.length; i++) {
            if (options[i].selected) {
                categoryIDs.push(Number(options[i].id));
            }
        }

        this.setState({ selectedCategories: categoryIDs })
    }

    handleSave = () => {
        const { dispatch, match, selectedProduct } = this.props;
        const {
            selectedCategories,
            expirationDate,
            name,
            brand,
            rating,
            itemsInStock,
            receiptDate,
        } = this.state;
        const errName = validateName(name);
        const errCategories = validateCategories(selectedCategories);
        const errDateExpiration = validateExpirationDate(expirationDate);

        if (errName || errCategories || errDateExpiration) {
            this.setState({ errName, errCategories, errDateExpiration });
        } else {
            const productId = match.params.id;
            const { createdAt } = selectedProduct;
            const productValues = {
                id: productId,
                name,
                brand,
                rating,
                featured: rating > 8,
                itemsInStock,
                receiptDate,
                categories: selectedCategories,
                expirationDate,
                createdAt,
            }

            dispatch(requestSaveProduct(productId, productValues));

            this.setState({ errName, errCategories, errDateExpiration, isProductChanged: true });
        }
    };

    renderRatingOptions = () => {
        const optionsLength = [...Array(9).keys()];

        return optionsLength.map(o =>
            <option value={o + 1} key={`option-${o + 1}`}>
                {o + 1}
            </option>
        );
    }

    render() {
        const { categories } = this.props;
        const {
            errName,
            errCategories,
            errDateExpiration,
            isProductChanged,
            name,
            brand,
            rating,
            itemsInStock,
            selectedCategories,
            receiptDate,
            expirationDate,
        } = this.state;

        return (
            <div className="mb-5">
                <h2 className="h3">Edit product</h2>
                <hr/>
                <Form className="product-form">
                    <FormGroup>
                        <Label for='name'>Name</Label>
                        <Input
                            value={name}
                            name='name'
                            id='name'
                            placeholder='Name'
                            innerRef={(node) => (this.inputName = node)}
                            onChange={this.handleInputChange}
                        />
                        <FormText color='danger'>{errName}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label for='brand'>Brand</Label>
                        <Input
                            value={brand}
                            name='brand'
                            id='brand'
                            placeholder='Brand'
                            onChange={this.handleInputChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for='rating'>Rating</Label>
                        <Input
                            value={rating}
                            type='select'
                            name='rating'
                            id='rating'
                            onChange={this.handleInputChange}
                        >
                            {this.renderRatingOptions()}
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for='itemsInStock'>Number in stock</Label>
                        <Input
                            type='number'
                            name='itemsInStock'
                            onChange={this.handleInputChange}
                            id='itemsInStock'
                            innerRef={(node) => (this.inputNumberInStock = node)}
                            value={itemsInStock}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for='categories'>Categories</Label>
                        <Input
                            type='select'
                            name='selectedCategories'
                            id='categories'
                            onChange={this.handleCategoryChange}
                            multiple={true}
                        >
                            {categories.map((category) => (
                                <option
                                    key={category.id}
                                    id={category.id}
                                    selected={selectedCategories.includes(Number(category.id))}
                                >
                                    {category.name}
                                </option>
                            ))}
                        </Input>
                        <FormText color='danger'>{errCategories}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Receipt Date</Label>
                        <DateTimePicker
                            time={false}
                            value={receiptDate}
                            onChange={(value) => this.setState({ receiptDate: value })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Expiration Date</Label>
                        <DateTimePicker
                            time={false}
                            value={expirationDate}
                            onChange={(value) => this.setState({ expirationDate: value })}
                        />
                        <FormText color='danger'>{errDateExpiration}</FormText>
                    </FormGroup>
                    <Row form>
                        <Col md={2}>
                            <Button
                                id="saveBtn"
                                className="btn btn-success"
                                onClick={this.handleSave}
                            >
                                Save
                            </Button>
                        </Col>
                        <Col md={5}>
                            {isProductChanged
                                ? <div>
                                    <p>The product's changed!</p>
                                    <Link id="backLink" className="product-form" to="/">Back</Link>
                                </div>
                                : null
                            }
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        selectedProduct: state.selectedProduct,
        categories: state.categories,
    };
};

export default connect(mapStateToProps)(EditForm);
