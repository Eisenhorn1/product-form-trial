import { productApi } from '../gateways/ProductApi';

export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';
export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';
export const ADD_PRODUCT_LOADING = 'ADD_PRODUCT_LOADING';
export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';
export const ADD_PRODUCT_FAIL = 'ADD_PRODUCT_FAIL';

export const REQUEST_PRODUCT_BY_ID = 'REQUEST_PRODUCT_BY_ID';
export const RECEIVE_PRODUCT_BY_ID = 'RECEIVE_PRODUCT_BY_ID';

export const SAVE_PRODUCT_START = 'SAVE_PRODUCT_START';
export const SAVE_PRODUCT_SUCCESS = 'SAVE_PRODUCT_SUCCESS';

export const DELETE_PRODUCT_START = 'DELETE_PRODUCT_START';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';

const requestProducts = () => ({
  type: REQUEST_PRODUCTS,
});

const receiveProducts = (json) => ({
  type: RECEIVE_PRODUCTS,
  products: json.map(product => product),
});

export const fetchProducts = () => dispatch => {
  dispatch(requestProducts());
  const json = productApi.getProducts();
  dispatch(receiveProducts(json));
};

const addProductStart = () => ({
  type: ADD_PRODUCT_LOADING
});

const addProductSuccess = (data) => ({
  type: ADD_PRODUCT_SUCCESS,
  newProduct: data
});

export const addProduct = (data) => dispatch => {
  dispatch(addProductStart());
  productApi.addProduct(data);
  dispatch(addProductSuccess(data));
};

const requestProductById = () => ({
  type: REQUEST_PRODUCT_BY_ID,
});

const receiveProductById = (data) => ({
  type: RECEIVE_PRODUCT_BY_ID,
  product: data
});

export const fetchProductById = id => dispatch => {
  dispatch(requestProductById());
  const data = productApi.getProductById(id);
  dispatch(receiveProductById(data));
};

const saveProductStart = () => ({
  type: SAVE_PRODUCT_START
});

const saveProductSuccess = (id, newData) => ({
  type: SAVE_PRODUCT_SUCCESS,
  newData,
  id
})

export const requestSaveProduct = (id, newData) => dispatch => {
  dispatch(saveProductStart());
  productApi.saveProduct(id, newData);
  dispatch(saveProductSuccess(id, newData));
};

const deleteProductStart = () => ({
  type: DELETE_PRODUCT_START
});

const deleteProductSuccess = (id) => ({
  type: DELETE_PRODUCT_SUCCESS,
  id
});

export const deleteProduct = (id) => dispatch => {
  dispatch(deleteProductStart());
  productApi.deleteProduct(id);
  dispatch(deleteProductSuccess(id));
};