import products from '../mocks/products';

class ProductApi {
    getProducts = () => {
        return products;
    }

    addProduct = product => {
        products.push(product)
    }

    getProductById = id => products.filter(product => product.id === Number(id))[0];

    saveProduct = (id, newData) => {
        const productToUpdateIndex = products.findIndex(product => product.id === Number(id));

        if (productToUpdateIndex !== -1) {
            products[productToUpdateIndex] = newData;
        }
    }

    deleteProduct = (id) => {
        const productToDeleteIndex = products.findIndex(product => product.id === Number(id));
        if (productToDeleteIndex !== -1) {
            products.splice(productToDeleteIndex, 1);
        }
    }
};

export const productApi = new ProductApi();
