import React from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'
import Main from './components/Main/Main'
import ProductsContainer from './components/Products/ProductsContainer'
import CreateForm from './components/CreateForm/CreateForm'
import EditForm from './components/EditForm/EditForm'
import NotFound from './components/NotFound/NotFound'


export function getRoutes() {
    return (
        <HashRouter>
            <Main>
                <Switch>
                    <Route exact path="/" component={ProductsContainer}/>,
                    <Route exact path="/create" component={CreateForm}/>,
                    <Route exact path="/edit/:id" component={EditForm}/>,
                    <Route path="*" component={NotFound}/>,
                </Switch>
            </Main>
        </HashRouter>
    )
}

export default getRoutes
