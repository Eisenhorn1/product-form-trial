import * as productsActions from '../actions/products';

export function selectedProduct(state = {}, action) {
  switch (action.type) {
    case productsActions.RECEIVE_PRODUCT_BY_ID:
      return action.product || state;
    default:
      return state;
  }
}
