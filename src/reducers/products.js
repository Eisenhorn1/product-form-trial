import * as productsActions from "../actions/products";

export function products(state = [], action) {
  switch (action.type) {
    case productsActions.RECEIVE_PRODUCTS:
      return [...action.products];
    case productsActions.ADD_PRODUCT_SUCCESS:
      return [action.newProduct, ...state];
    case productsActions.SAVE_PRODUCT_SUCCESS:
      const productToUpdateIndex = state.findIndex(product => product.id ===Number(action.id));
      const newProducts = Array.from(state);

      if (productToUpdateIndex !== -1) {
        newProducts[productToUpdateIndex] = action.newData;
    }
      return newProducts;
    case productsActions.DELETE_PRODUCT_SUCCESS:
      const productToDeleteIndex = state.findIndex(product => product.id ===Number(action.id));
      const productsCopy = Array.from(state);
      
      productsCopy.splice(productToDeleteIndex, 1);

      return productsCopy;
    default:
      return state;
  }
}
