import { combineReducers } from 'redux';
import { categories } from './categories';
import { products } from './products';
import {selectedProduct} from './selectedProduct';

export default combineReducers({
  categories,
  products,
  selectedProduct
});
