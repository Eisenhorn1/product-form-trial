describe('Creation test', () => {
    it('should visit localhost', function () {
        cy.visit('http://localhost:3000/#/')
    });

    it('should go to edit form', function () {
        cy.get('#goToCreate').click()
    });

    it('should fill form with errors', function () {
        cy
            .get('input[name="name"]')
            .type('item with a very long name item with a very long name item with a very long name item with a very long name item with a very long name item with a very long name item with a very long name item with a very long name');
        cy
            .get('input[name="brand"]')
            .type('JBL');
        cy
            .get('select[name="categories"]')
            .select(['TV & Home Cinema', 'Laptops', 'Watches', 'Headphones', 'Phones', 'Desktops']);
        cy
            .get('#createBtn')
            .click();
    });

    it('should correct form inputs', function () {
        cy
            .get('input[name="name"]')
            .clear()
            .type('Pulse 3');
        cy
            .get('select[name="categories"]')
            .select('Audio & HiFi');
        cy
            .get('#rw_2_input')
            .clear();
        cy
            .get('#createBtn')
            .click();
        cy
            .get('#backLink')
            .click();
    });
});

describe('Edit test', () => {
    it('should open an item', function () {
        cy
            .get('.card')
            .eq(1)
            .find('a')
            .click();
    });

    it('should change item info', function () {
        cy
            .get('select[name="rating"]')
            .select('9');
        cy
            .get('#saveBtn')
            .click();
    });

    it('should back to home page', function () {
        cy
            .get('#backLink')
            .click();
    });
});
